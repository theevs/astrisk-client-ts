import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Call } from './call';
import { environment } from './environment';

import 'rxjs/add/operator/toPromise';

const header: Headers = new Headers({'Content-Type': 'application/json'});
const options: RequestOptions = new RequestOptions({headers: header});

@Injectable()
export class CallsService {

  private _calls: Subject<Call[]> = new Subject();
  private callsUrl: string = environment.callsUrl;
  private dataStore: { calls: Call[] };
  
  constructor(private http: Http) { 
    this.dataStore = { calls: [] };
  };

  get calls() {
    return this._calls.asObservable();
  }

  create(call: Call) {
    call.id = this.dataStore.calls.length + 1;
    this.dataStore.calls.push(call);
    this._calls.next(this.dataStore.calls);
    return this.post(call)
      .then((call: Call) => {
        this.dataStore.calls.forEach((_call, i) => {
          if (_call.id === call.id) {
            let nCall = Call.fromObj(call);
            this.dataStore.calls[i] = nCall;
          }
        });
        this._calls.next(this.dataStore.calls);
      })
      .catch(() => {
        this.dataStore.calls.forEach((_call, i) => {
          if (_call.id === call.id) {
            this.dataStore.calls[i].state = 15;
          }
        });
        return Promise.reject(new Error('Не удалось отправить на сервер'));
      })
  }

  private post(call: Call) {
    return this.http.post(this.callsUrl, JSON.stringify(call), options)
      .toPromise()
      .then((res: Response) => res.json() as Call)
      .catch(error => {
        console.error('Не удалось создать звонок: ', error);
        return  Promise.reject(error.message || error);
      })
  }

}
