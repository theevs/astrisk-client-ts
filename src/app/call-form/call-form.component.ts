import { Component } from '@angular/core';
import { Call } from '../call';
import { CallsService } from '../calls.service';

@Component({
  moduleId: module.id,
  selector: 'app-call-form',
  templateUrl: 'call-form.component.html',
  styleUrls: ['call-form.component.css']
})
export class CallFormComponent {

  model: Call = new Call();
  error: string;

  constructor(private callService: CallsService) { }

  create() {
    this.callService.create(this.model)
      .then( () => this.error = null )
      .catch(err => this.error = err);

    this.model = new Call();
  }

}
