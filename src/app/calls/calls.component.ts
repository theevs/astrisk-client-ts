import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { Call } from '../call';
import { CallsService } from '../calls.service';

@Component({
  moduleId: module.id,
  selector: 'app-calls',
  templateUrl: 'calls.component.html',
  styleUrls: ['calls.component.css']
})
export class CallsComponent implements OnInit {

  calls: Observable<Call[]>;

  constructor(private callService: CallsService) { }

  ngOnInit() {
    this.calls = this.callService.calls;
  }

}
