const statuses = {
  0: 'Номер или экстеншен не найден',
  1: 'На другом конце повесили трубку',
  2: 'Ошибка соединения',
  3: 'Звонок',
  4: 'Успешное выполнение',
  5: 'Номер занят',
  7: 'Линия не доступна',
  8: 'Линия перегружена',

  15: 'Ошибка отправки'
}

export class Call {
  constructor(public fromId?: string,
              public toId?: string,
              public actionId?: string,
              public state?: number,
              public id?: number) {}

  static fromObj(obj: Call): Call {
    return new Call(obj.fromId, obj.toId, obj.actionId, obj.state, obj.id);
  }

  get status(): string {
    return this.state != undefined ? statuses[this.state] : 'Отправка';
  }

  get style()
  {
    let res: string;
    switch (this.state) {
      case 4:
      case 3:
        res = 'success';
        break;
      case 1:
      case 5:
      case 7:
      case 8:
        res = 'warning';
        break;
      case 0:
      case 2:
      case 15:
        res = 'danger'
        break;
      default:
        res = 'info';
        break;
    }
    return res;
  }

}
