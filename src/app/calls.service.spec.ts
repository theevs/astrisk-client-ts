/* tslint:disable:no-unused-variable */

import { addProviders, async, inject } from '@angular/core/testing';
import { CallsService } from './calls.service';

describe('Service: Calls', () => {
  beforeEach(() => {
    addProviders([CallsService]);
  });

  it('should ...',
    inject([CallsService],
      (service: CallsService) => {
        expect(service).toBeTruthy();
      }));
});
