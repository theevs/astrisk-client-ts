import { Component } from '@angular/core';

import { CallFormComponent } from './call-form/';
import { CallsComponent } from './calls/';
import { CallsService } from './calls.service';


@Component({
  moduleId: module.id,
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css'],
  directives: [CallFormComponent, CallsComponent],
  providers: [CallsService]
})
export class AppComponent {
}
