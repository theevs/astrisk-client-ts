import { AsteriskClientPage } from './app.po';

describe('asterisk-client App', function() {
  let page: AsteriskClientPage;

  beforeEach(() => {
    page = new AsteriskClientPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
